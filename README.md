This solution was built using .NET Core (1.0.4) and Visual Studio Code

Also tested in Visual Studio for Mac :)

To run from the command line:
```
dotnet restore && dotnet run
```
﻿using System;

namespace miac
{
    class InterviewQuestions
    {
        public int GetSum(int a, int b) 
        {
            int sum = 0;
            int x = a, y = b;

            if (a == b) return a;

            // sort args for loop 
            if (a > b) { x = b; y = a; }

            for (var i = x; i <= y; i++)
                sum += i;

            return sum;
        }

        public bool IsAscOrder(int[] arr)
        {
            for (var i = 0; i < arr.Length - 1; i++) 
            {
                if (arr[i] < arr[i + 1]) 
                    continue;
                else
                    return false;
            }
            return true;
        }

        public int FindEvenIndex(int[] arr) 
        {
            int sumLeft = 0, sumRight;

            for (var i = 0; i < arr.Length; i++) 
            {
                sumLeft += arr[i];
                sumRight = 0;

                for (var j = i + 2; j < arr.Length; j++) 
                    sumRight += arr[j];

                if (sumLeft == sumRight) 
                    return (i + 1);
            }
            return -1;
        }

        public int CalculateYears(double principal, double interestRate, double taxRate, double desiredPrincipal) 
        {
            int years = 0;
            double interest, taxAmount;
            
            if (principal == desiredPrincipal) return 0;

            while (true) {
                years += 1;

                interest = principal * interestRate;
                taxAmount = Math.Round(interest, 2) * taxRate;
                principal += (interest - Math.Round(taxAmount, 2));

                if (Math.Round(principal, 2) >= desiredPrincipal)
                    return years;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var questions = new InterviewQuestions();

            // 1.
            Console.WriteLine(questions.GetSum(1, 0));
            Console.WriteLine(questions.GetSum(1, 2));
            Console.WriteLine(questions.GetSum(0, 1));
            Console.WriteLine(questions.GetSum(1, 1));
            Console.WriteLine(questions.GetSum(-1, 0));
            Console.WriteLine(questions.GetSum(-1, 2));
            
            // 2.
            int[] arr1 = {1, 2, 3};
            int[] arr2 = {3, 2, 1};
            Console.WriteLine(questions.IsAscOrder(arr1));
            Console.WriteLine(questions.IsAscOrder(arr2));

            // 3.
            int[] arr3 = {1, 2, 3, 4, 3, 2, 1};
            int[] arr4 = {1, 100, 50, -51, 1, 1};
            Console.WriteLine(questions.FindEvenIndex(arr3));
            Console.WriteLine(questions.FindEvenIndex(arr4));

            // 4.
            Console.WriteLine(questions.CalculateYears(1000, 0.05, 0.18, 1100));
        }
    }
}
